package primes;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//I SOLVED THE GIVEN PROBLEM BY USING DYNAMIC PROGRAMMING
		ArrayList<Integer> primes=new ArrayList<Integer>();//used to store all primes to be used later in calculating bigger primes(bottom-up approach)

		//first, all the primes less than 500 are added to list
		for(int number=2;number<500;number++) {
			//if number is prime, add it to prime numbers list, so dynamically update the list.
			if(isNumberPrime(primes,number)) primes.add(number);
		}
		
		//Now it is time for the real task, finding 3 digit prime numbers starting with 5(primes between 500 and 600)
		for(int i=500;i<600;i++) {
			if(isNumberPrime(primes,i)) {
				System.out.println(i);
				primes.add(i);
			}
		}

		
		
	}
	
	public static boolean isNumberPrime(ArrayList<Integer> primes, int number) {
		boolean num_is_prime=true;
		//check if a number can be divided by any prime number less than itself
		//if it can be divided, it is not prime
		//if it can't be divided, then it is prime
		for (int i=0;i<primes.size();i++) {
			if(number%(int)primes.get(i)==0) {
				num_is_prime=false;
				break;
			}
		}
		return num_is_prime;
	}

}
